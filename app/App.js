import React, { Component } from 'react';
import Toggle from './components/Toggle';

export default class App extends Component {
    state = {
        toggle: false,
    }

    render(){
        return (
            <Toggle
                active={this.state.toggle}
                name="toggle"
                onChange={this.handleToggleChange}
            />
        );
    }

    handleToggleChange = e => {
        const { name, active } = e.target;

        this.setState({
            [name]: active,
        });
    };
}

