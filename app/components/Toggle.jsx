import React, { Component, PropTypes } from 'react';

export default class Toggle extends Component {
    static propTypes = {
        active: PropTypes.bool.isRequired,
        labelOff: PropTypes.string.isRequired,
        labelOn: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
    };

    static defaultProps = {
        active: false,
        labelOff: 'Off',
        labelOn: 'On',
        name: '',
    };

    render(){
        return (
            <button onClick={this.handleClick}>
                {this.renderLabel()}
            </button>
        );
    }

    renderLabel(){
        const {
            active,
            labelOn,
            labelOff,
        } = this.props;

        return active ? labelOn : labelOff;
    }

    handleClick = e => {
        e.preventDefault();

        const { name, active } = this.props;

        this.props.onChange({
            target: {
                name,
                active: !active,
            },
        });
    };
}
