import React from 'react';
import { shallow } from 'enzyme';

import Toggle from '../Toggle';

describe('Toggle', () => {
    let wrapper, props;

    function getButton(){
        return wrapper.find('button').first();
    }

    beforeEach(() => {
        props = {
            name: 'test',
            active: false,
            onChange: jest.fn(),
        };

        wrapper = shallow(
            <Toggle
                {...props}
            />
        );

    });

    it('renders the default label', () => {
        expect(getButton().text()).toBe('Off');
    });

    it('renders custom label', () => {
        const labelOff = 'deactivated';

        wrapper.setProps({ labelOff });

        expect(getButton().text()).toBe(labelOff);
    });

    it('calls the onChange callback', () => {
        const preventDefault = jest.fn();

        getButton().simulate('click', {
            preventDefault,
        });

        expect(preventDefault).toHaveBeenCalled();
        expect(props.onChange).toHaveBeenCalledWith({
            target: {
                name: props.name,
                active: !props.active,
            },
        });
    });

    describe('when active', () => {

        beforeEach(() => {
            wrapper.setProps({ active: true });
        });

        it('renders the default label', () => {
            expect(getButton().text()).toBe('On');
        });

        it('renders custom label', () => {
            const labelOn = 'active';

            wrapper.setProps({ labelOn });

            expect(getButton().text()).toBe(labelOn);
        });
    });

});
