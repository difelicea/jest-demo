import React from 'react';
import { shallow } from 'enzyme';

import App from '../App';

describe('App', () => {
    let wrapper, toggleProps;

    function getToggleProps() {
        return wrapper.find('Toggle').first().props();
    }

    beforeEach(() => {
        // Default
        toggleProps = {
            name: 'toggle',
            active: false,
        };

        wrapper = shallow(
            <App />
        );
    });

    it('renders Toggle component', () => {
        expect(getToggleProps()).toBeTruthy();
        expect(getToggleProps().active).toBe(toggleProps.active);
        expect(getToggleProps().name).toBe(toggleProps.name);
    });

    it('handles the "toggle" state', () => {
        getToggleProps().onChange({
            target: toggleProps,
        });

        expect(getToggleProps().active).toBe(toggleProps.active);
    });
});
